-- // create_changelog
-- Migration SQL that makes the change goes here.
CREATE TABLE ${changelog} (
  ID NUMERIC(20,0) NOT NULL primary key,
  APPLIED_AT VARCHAR(25) NOT NULL,
  DESCRIPTION VARCHAR(255) NOT NULL
);

-- //@UNDO
-- SQL to undo the change goes here.

DROP TABLE ${changelog};
