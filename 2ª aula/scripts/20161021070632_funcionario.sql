--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // funcionario
-- Migration SQL that makes the change goes here.
create table funcionario(
	id int primary key auto_increment,
    nome varchar(80),
    cpf varchar(11) unique not null,
    cargo varchar(80),
    email varchar(60),
    setor varchar(80),
    telefone varchar(12),
    usuario varchar(10) unique not null,
    senha varchar(10) not null,
    admin boolean,
    horario_id int references horario(id)
);
-- //@UNDO
-- SQL to undo the change goes here.
drop table funcionario;